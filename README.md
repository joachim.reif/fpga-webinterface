# FPGA Webinterface

A Webinterface created with Django to access the IDS FPGA-Cluster.

To transfer the Program to a Linux-Systems following steps are necessary

1. Copy the folders "fpga_interface" and "textfiles" to the directory "digitalbrain/src/server/"
2. Create Python environment from Textfile "django_env.txt"
3. Start Python environment
4. Open file "fpga_interface/fpga_interface/settings.py"
    -import os
    -Add the ip of the server to the list of "ALLOWED_HOSTS" as string
    -Above "STATIC_URL" copy STATIC_ROOT = os.path.join(BASE_DIR , "static")
5. Open file "fpga_interface/interface/views.py"
    -change "PYTHON_PROGRAM = FOLDER_PATH / "plot.py"" (line 38) to "PYTHON_PROGRAM = Path.cwd().parents[1] / "sw" / "examples" / "n_rand_neurons.py"" (line 39)
    -change "data_list.append(str(PurePath.as_posix(FOLDER_PATH / "output" / element)))" (line 289) to "data_list.append(str(PurePath.as_posix(OUTPUT_PATH / element)))" (line 290)
6. Open file "fpga_interface/fpga_cli/cli.py"
    -uncomment the fpga commands
7. Open shell in directory "fpga_interface"
8. Run command "python manage.py collectstatic"
9. Start the server with "python manage.py runserver 0.0.0.0:8080
10. Open browser and use url "http://<ip>:8080/"
