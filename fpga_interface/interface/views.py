from ast import Param
import json
import math
from telnetlib import STATUS

from django.http import HttpResponse
from django.shortcuts import render, redirect
#from django.template import loader
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_safe, require_POST
from django.contrib.auth import logout
from django.urls.base import reverse
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.utils.translation import ugettext as _
from django.contrib.sites.shortcuts import get_current_site

import ctypes as ct
import json
import os
from pathlib import Path, PurePath

import yaml
from io import open as iopen

from interface.validators import validate_settings, validate_request_data, validate_start_message, validate_selected_boards
from django.core.exceptions import ValidationError


FOLDER_PATH = Path.cwd().parents[0] / "textfiles"
SETTINGS_FILE = "config.yaml"
SOURCE_PATH = str(Path.cwd().parents[1])
OUTPUT_PATH = str(Path.cwd().parents[1] / "output")


BOARDS_LIST = []

number_of_time_steps = 0


@login_required
def base_view(request):
    return render(request, "interface/base.html")


@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, _(
                'Your password was successfully updates!'))
            return redirect(reverse("interface:base"))
        else:
            messages.error(request, _('Please correct the error below.'))
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'interface/password_change.html', {'form': form})


@login_required
def logout_view(request):
    print("Logout!!")
    logout(request)
    return redirect(reverse("login:login"))

def propagator_32(tau_syn, tau, c, h):
    P32_LINEAR = 1 / (2 * c * tau * tau) * h * h * (tau_syn - tau) * math.exp(-h / tau)
    P32_SINGULAR = h / c * math.exp( -h / tau)
    P32 = -tau / (c * (1 - tau / tau_syn)) * math.exp(-h / tau_syn) * (math.exp( h * ( 1 / tau_syn -1 / tau)) -1)

    DEV_P32 = abs(P32 - P32_SINGULAR)

    if(tau == tau_syn or (abs(tau-tau_syn) < 0.1 and DEV_P32 > 2 * abs(P32_LINEAR))):
        return P32_SINGULAR
    else:
        return P32

def write_data_to_configfile(data):

    with open(FOLDER_PATH / SETTINGS_FILE) as configfile:
        configuration = yaml.safe_load(configfile)

    configuration["Scheduler"].update({"Syncs per Timestep": int(data["syncsPerTimeStep"])})
    configuration["Scheduler"].update({"Timestep Limit": int(data["timestepLimit"])})

    number_of_time_steps = int(data["timestepLimit"])
    
    neuron_dict = configuration["Neuron"]

    #Set Params
    neuron_dict.update({"V_thresh": float(data["v_thresh_mv"])})
    neuron_dict.update({"V_reset": float(data["v_reset_mv"])})
    neuron_dict.update({"T_ref": float(data["t_ref_ms"])})
    neuron_dict.update({"P11_exc" : math.exp(float(data["h_ms"])*-1/ float(data["tau_ex_ms"]))})
    neuron_dict.update({"P11_inh" : math.exp(float(data["h_ms"])*-1/ float(data["tau_in_ms"]))})
    neuron_dict.update({"P22" : math.exp(float(data["h_ms"])*-1/ float(data["tau_ms"]))})
    neuron_dict.update({"P20" : float(data["tau_ms"]) / float(data["c_pf"]) * (1.0 - neuron_dict["P22"])})
    neuron_dict.update({"P21_exc" : propagator_32(float(data["tau_ex_ms"]), float(data["tau_ms"]), float(data["c_pf"]), float(data["h_ms"]))})
    neuron_dict.update({"P21_inh" : propagator_32(float(data["tau_in_ms"]), float(data["tau_ms"]), float(data["c_pf"]), float(data["h_ms"]))})
    neuron_dict.update({"Exc Weight Decimal Places": int(data["weightFracExc"])})
    neuron_dict.update({"Inh Weight Decimal Places": int(data["weightFracInh"])})
    

    configuration["Neuron"].update(neuron_dict)

    with iopen(FOLDER_PATH / SETTINGS_FILE, "w", encoding="utf-8") as out:
        yaml.dump(configuration, out, default_flow_style=False, sort_keys=False, allow_unicode=True)

@login_required
@require_POST
def set_network_params(request):
        request_data = json.loads(request.body.decode("utf-8"))

        #Validate Data
        try:
            validate_request_data(request_data)
        except ValidationError as e:
            print(e)
            return HttpResponse(e.message, status=400)
        try:
            for element in request_data:
                validate_settings(request_data[element], element)
        except ValidationError as e:
            print(e.message)
            return HttpResponse(e.message, status=400)
        try:
            validate_selected_boards(request_data["Boards"], BOARDS_LIST)
        except ValidationError as e:
            print(e.message)
            return HttpResponse(e.message, status=400)

        write_data_to_configfile(request_data)
        print("Write settings to configfile.")

        initCluster(request_data["Boards"])
            
        return HttpResponse("set_parameters", status=200)

def get_surounding_boards(board_list):
    first_board = board_list[0]
    last_board = board_list[-1]

    ret = []

    for index, row in enumerate(BOARDS_LIST):
        if first_board in row:
            for j, element in enumerate(row):
                if element == first_board:
                    top_left_corner = (index, j)

        if last_board in row:
            for j, element in enumerate(row):
                if element == last_board:
                    bottom_right_corner = (index, j)

    if top_left_corner[0] == 0 and top_left_corner[1] == 0 and bottom_right_corner[0]+1 == len(BOARDS_LIST) and bottom_right_corner[1]+1 == len(BOARDS_LIST[0]) :
        return [];
    
    cur_pos = bottom_right_corner[0]

    if len(BOARDS_LIST)-(bottom_right_corner[0]+1-top_left_corner[0]) == 1:
        ret.extend(BOARDS_LIST[top_left_corner[0]-1][top_left_corner[1]:bottom_right_corner[1]+1])
    elif len(BOARDS_LIST)-(bottom_right_corner[0]+1-top_left_corner[0]) > 1:
        ret.extend(BOARDS_LIST[top_left_corner[0]-1][top_left_corner[1]:bottom_right_corner[1]+1])

        if(bottom_right_corner[0]+1 == len(BOARDS_LIST)):
            cur_pos = -1;
        ret.extend(BOARDS_LIST[cur_pos+1][top_left_corner[1]:bottom_right_corner[1]+1])

    cur_pos = bottom_right_corner[1]
    if len(BOARDS_LIST[0])-(bottom_right_corner[1]+1-top_left_corner[1]) == 1:
        ret.extend([r[top_left_corner[1]-1] for r in BOARDS_LIST[top_left_corner[0]: bottom_right_corner[0]+1]])
    elif len(BOARDS_LIST[0])-(bottom_right_corner[1]+1-top_left_corner[1]) > 1:
        ret.extend([r[top_left_corner[1]-1] for r in BOARDS_LIST[top_left_corner[0]: bottom_right_corner[0]+1]])

        if(bottom_right_corner[1]+1 == len(BOARDS_LIST[0])):
            cur_pos = -1

        ret.extend([r[cur_pos+1] for r in BOARDS_LIST[top_left_corner[0]: bottom_right_corner[0]+1]])

    #print(ret)
    return ret

def initCluster(boardList):
    #print(f"{Path.cwd().parents[1]=}")
    #print("Boards: {}".format(boardList))
    #source_path = str(Path.cwd().parents[1])

    row = None
    for l in BOARDS_LIST:
        if boardList[-1] in l:
            row = l
            break

    for element in row:
        if element in boardList:
            main_node = element
            break


    surrounding_boards = get_surounding_boards(boardList)
    
    boards = "{} {}".format(" ".join(boardList), " ".join(surrounding_boards))
    command = " ".join(["cd", SOURCE_PATH, "&&" , "./fpga.sh --targets", boards, "--program develop"])
    print(command)
    #os.popen(command)
    command = " ".join(["cd", SOURCE_PATH, "&&", "./fpga.sh --targets", " ".join(boardList), "--upload embedded"])
    print(command)
    #os.popen(command)
    command = " ".join(["cd", SOURCE_PATH, "&&", "export TARGET="+main_node])
    print(command)
    #os.popen(command)

    # command = " ".join(["cd", SOURCE_PATH, "&&", "./fpga.sh --targets", boards, "--program develop", "&&", "./fpga.sh --targets", " ".join(boardList), "--upload embedded", "&&" , "export TARGET="+main_node, "&&"])



@login_required
@require_POST
def start_simulation(request):
    message = request.body.decode("utf-8")
    try:
        validate_start_message(message)
    except ValidationError:
        return HttpResponse("incorrect startmessage", status=400)

    command = " ".join(["cd", SOURCE_PATH, "&&", "./sim", str(number_of_time_steps), "src/sw/examples/n_rand_neurons.py --neurons 32 --init"])
    print(command)
    #os.popen(command)

    #calculate_metrics()
    #return HttpResponse(True, status=200)
    url = "http://{0}{1}".format(get_current_site(request), reverse("output:output"));
    return HttpResponse(url, status=200)

def get_files_from_folder(folder, file_prefix, file_suffix):
    file_list = os.listdir(folder)
    data_list = []
    for element in file_list:
        if element.startswith(file_prefix) and element.endswith(file_suffix):
            data_list.append(str(PurePath.as_posix(FOLDER_PATH / "output" / element)))
    return data_list

def calculate_metrics():
    file_list = get_files_from_folder(FOLDER_PATH / "output", "read_spikes_node", "bin")
    command = "cd \"{}\" & merger.exe".format(str(PurePath.as_posix(FOLDER_PATH/ "programs")))

    cur_path = Path.cwd();

    for file in file_list:
        command += " \"{}\"".format(file)
    command += " -o \"{}\"".format(str(PurePath.as_posix(FOLDER_PATH / "output")))

    command += " & cd \"{}\"".format(str(PurePath.as_posix(cur_path)))
    print(command)

    res = os.popen(command)
    print(res.read())

    #print("NumFiles: {0}, Files: {1}".format(num_files, file_str));


    library_path = FOLDER_PATH / "programs" / "createMetrics.so"
    __creater = ct.CDLL(str(library_path))
    __creater.create.restype = ct.c_int
    __creater.create.argstypes = [ct.c_int, ct.c_char_p, ct.c_char_p]

    num_files = 1
    file_str = str(FOLDER_PATH/ "output" / "spikes.bin")
    req_files = file_str.encode("utf-8")

    dir = str(FOLDER_PATH / "output").encode("utf-8")

    ret = __creater.create(num_files, req_files, dir)
    print(ret)

@login_required
@require_safe
def read_cluster_informations(request):
    if request.method == "GET":
        path = FOLDER_PATH / "clusterlayout.txt"
        ret = {}

        global BOARDS_LIST
        with open(path, "r") as file:
            for line in file:
                if "#" in line:
                    continue
                if "names:" in line:
                    for line in file:
                        BOARDS_LIST.append([])
                        content = line.split(",")
                        for element in content:
                            element = element.replace("\n", "")
                            BOARDS_LIST[-1].append(element)
                    break;
                content = line.partition(":")
                ret.setdefault(content[0].strip().lower(), content[2].strip().lower())
        ret.setdefault("names", BOARDS_LIST)
        json_file = json.dumps(ret, indent=4)

        return HttpResponse(json_file, status=200)
    return HttpResponse("wrong method", status=405)

@login_required
@require_safe
def get_connections_infomations(request):
    if request.method == "GET":
        ret = {}

        # response = os.popen("");
        # response = [zeile.strip() for zeile in response]
        # for line in response: 
        #     if "Neighbour" in line:
        #         infos = line.split(" ")
        #         if len(infos) <7:
        #             break
        #         startIndex = 3 if len(infos)==8 else 2;
        #         if "(255|255)" in infos[startIndex]:
        #             continue
            
        #         ret.setdefault(line[startIndex], [infos[startIndex+1].rstrip(",").strip(), infos[startIndex+2].rstrip(",").strip(), infos[startIndex+3].rstrip(",").strip(), infos[startIndex+4].rstrip(",").strip()])
        ret.setdefault("(100|104)", ["1274", "0", "0", "300"])
        return HttpResponse(json.dumps(ret, indent=4), status=200);
    return HttpResponse("wrong method", status=405)

@login_required
def get_tempreture():
    # response = os.popen("./run.sh --node 100 100 read temperature"); TODO
    response = "Node (100|100): 56.968°C\nDisconnecting\ndone!"
    start = response.rfind("Node")
    end = response.find("C", start+14)
    temp = (response[start:end+1]).spilt(":")[1]
    temp = temp.strip();
    print(temp)

@login_required
@require_safe
def get_nest_code(request):
    programPath = FOLDER_PATH / "plot.py"

    with open(programPath, "r") as file:
        content = file.readlines();
    return HttpResponse(content);

@login_required
@require_POST
def set_nest_code(request):
    content = request.body.decode("utf-8")
    print(content)
    return HttpResponse("set code", status=200)