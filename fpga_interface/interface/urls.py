from django.urls import path
from . import views

app_name = 'interface'
urlpatterns = [
    path('', views.base_view, name="base"),
    path("logout/", views.logout_view, name="logout"),
    path("change_password/", views.change_password, name="changepw"),
    path("cluster_informations/", views.read_cluster_informations, name="clusterinformations"),
    path("connections_informations/", views.get_connections_infomations, name="connectionsinfromations"),
    path("network_settings/set/", views.set_network_params, name="network_set"),
    path("simulation/start/", views.start_simulation, name="simulation_start"),
    path("nestcode/", views.get_nest_code, name="nest_code"),
    path("setCode/", views.set_nest_code, name="set_code"),
]
