from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


DOWNLOADABLE_FILES = ["results_plain", "results_binary", "metric_pr", "metric_cv", "metric_pc"]

def validate_download_input(download_data : dict):
    if not len(download_data) <= len(DOWNLOADABLE_FILES):
        raise ValidationError("Too many arguments")

    for element in download_data.keys():
        if element not in DOWNLOADABLE_FILES:
            raise ValidationError("Not a valid file.")


STATISTICS = ["PR", "PC", "CV"]

def validate_statistic_input(statistic_data : dict):
    if not len(statistic_data) <= len(STATISTICS):
        raise ValidationError("Too many arguments")

    for element in statistic_data.keys():
        if element not in STATISTICS:
            raise ValidationError("Not a valid statistic.");