from django.urls import path
from . import views

app_name = 'output'
urlpatterns = [
    path("", views.output_view, name="output"),
    path("download/", views.download_data, name="download"),
    path("statistics/", views.get_statistics, name="getstatistics"),
]
