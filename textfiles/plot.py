import matplotlib.pyplot as plt
import sys

for filename in sys.argv[1:]:
    x= []
    y = []
    with open(filename) as file:
        for line in file:
            line_split = line.split()
            x.append(float(line_split[0]))
            y.append(float(line_split[1]))
    plt.plot(x, y, label=filename)


plt.legend()
plt.show()